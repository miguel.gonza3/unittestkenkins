package com.tesla.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.junit.Assert;

public class CalculateTest {

	@InjectMocks
	private Calculate calculate;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void sumTest() {

		int result = calculate.sum(1, 1);

		Assert.assertEquals(result, 2);
	}

}
