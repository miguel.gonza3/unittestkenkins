package com.tesla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitTestJenkinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnitTestJenkinsApplication.class, args);
	}

}
